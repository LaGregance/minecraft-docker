# Run the server

```bash
docker volume create mcdata # Create volume to store data
docker-compose up -d # Run the server in background
docker logs -f minecraft # Show logs
```

# Config the server

```bash
docker exec -ti minecraft bash
apt-get update && apt-get install -y vim
vim server.properties # Or any other file
```

# Execute a command in the server

```bash
docker exec -i minecraft rcon-cli
```

# Backup

```bash
# Manual backup (always pass absolute path)
./backup.sh -v mcdata -c minecraft -o /PATH_TO_YOUR_SERVER/backups -r false -d 7

# Cron job than run every 12h
0 0,12 * * * bash /PATH_TO_YOUR_SERVER/backup.sh -v mcdata -c minecraft -o /PATH_TO_YOUR_SERVER/backups -r false -d 7
```

# Thanks

All come from : https://www.youtube.com/watch?v=f1cGEr9L67Y